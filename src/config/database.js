// duplicado este require do dotenv pq o sequelize-cli não tem acesso ao app.js somente a este arquivo

require('dotenv').config({
  path: process.env.NODE_ENV === "test" ? ".env.test" : ".env"
});

module.exports = {
  host: process.env.DB_HOST,
  username: process.env.DB_USER,
  password: process.env.DB_PASS,
  database: process.env.DB_NAME,
  dialect: process.env.DB_DIALECT || "postgres",
  storage: './__tests__/database.sqlite', // caminho usado quando os testes usar o dialeto SQLITE
  operatorsAliases: false, // desabilitar um warning do sequelize
  logging: false, // não mostrar muitos logs enquanto está rodando as migrations
  define: {
    timestamps: true, // toda tabela tem os campos created_at e updated_at
    underscored: true, // cria tabela com snake_case
    underscoredAll: true // cria todos os campos das tabelas com snake_case
  }
}
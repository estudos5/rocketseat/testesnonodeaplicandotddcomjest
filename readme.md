# Testes no NodeJS aplicando TDD com Jest | Diego Fernandes

Jest, Factory Girl, Faker, Supertest, Sequelize, JWT, BCrypt

```
$ yarn add express
```

# Banco de dados

Usando um banco de dados relacional Postgres e o ORM Sequelize.
Instalar o pacote pg para o sequelize se comunicar com o Posgrees

```
$ yarn add sequelize pg
```
Instalar o sequelize-cli para ajudar na criação de migrations(arquivos que controlam as versões das tabelas no banco de dados)

```
$ yarn add sequelize-cli -D
```
Rodar o comando do sequelize para iniciar os arquivos
```
$ yarn sequelize init
```

## Container Postgrees
instanciando um [container postgres]()
```
$ docker run -p 5432:5432\
    --name postgres \
    -e POSTGRES_USER=docker \
    -e POSTGRES_PASSWORD=docker \
    -d postgres
```
testando o container
```
$ docker exec -it postgres bash
# psql -U docker -W
```
Criar instancia do [pgadmin4 container](https://www.pgadmin.org/docs/pgadmin4/latest/container_deployment.html)
```
docker run -p 80:80 \
    --name pgadmin4 \
    --link postgres \
    -e 'PGADMIN_DEFAULT_EMAIL=docker' \
    -e 'PGADMIN_DEFAULT_PASSWORD=docker' \
    -d dpage/pgadmin4
```

# Criando as tabelas com Migrations
Controle de versões do banco de dados

Rode o comando para criar os arquivos de migrations na pasta ./database/migrations, que foi configurada no arquivo .sequelizerc:
```
$ yarn sequelize migration:create --name=create-users
```
Rode o comando com os arquivos de migrations editados para executar as migrations e criar as tabelas:
```
$ yarn sequelize db:migrate
```
# Testes

Instalar o jest
```
$ yarn add jest -D
$ yarn jest --init


✔ Would you like to use Jest when running "test" script in "package.json"? … yes
✔ Choose the test environment that will be used for testing › node
✔ Do you want Jest to add coverage reports? … no
✔ Automatically clear mock calls and instances between every test? … yes
```

## Pastas de testes

* **unit**: testes unitários - funções puras - dado um valor, sempre tem o mesmo retorno e não tem efeito colateral(não chamam APIs, cadastro em banco de dados)
* **integration**: testes de integração - funções que têm efeito colateral. Podem fazer chamadas a APIs, banco de dados

## Criar arquivos de variáveis ambiente

Crir os arquivos .env e .env.test para armazenar as variáveis de ambiente.

Instalar o módulo dotenv para carregar os arquivos:
```
$ yarn add dotenv
```
neste estudo, é usado o sqlite com banco para executar os códigos de teste
```
$ yarn add sqlite3 -D
```
# Criando testes das rotas
Instalar o módulo supertest para testar as rotas:
```
$ yarn add supertest -D
```

# JWT - JSON WebToken

Instalando:
```
$ yarn add jsonwebtoken
```